import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import Camera from 'react-native-camera';


export default class App extends Component {

	render() {
		return (
			<View style={styles.container}>
				<Camera
				ref={(cam) => {
					this.camera = cam
				}}
				style={styles.camera}
				aspect={Camera.constants.Aspect.fill}>
					<Text
					style={styles.capture}
					onPress={this.takePicture.bind(this)}>
						Take Picture
					</Text>
				</Camera>
			</View>
		);
	}

	takePicture() {
		const options = {};

		this.camera.capture({metadata: options})
		.then((data) => {
			Alert.alert(data);
			console.log(data);
		}).catch((error) => {
			console.log(error);
		});
	}

}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#f5fcff',
	},
	camera: {
		flex: 1
	},
	capture: {
		fontSize: 20,
		textAlign: 'center',
		justifyContent: 'center',
		alignItems: 'center',
		margin: 10,
		color: '#fff',
		backgroundColor: '#000'
	}
});
